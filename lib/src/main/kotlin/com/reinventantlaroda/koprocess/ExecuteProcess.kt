package com.reinventantlaroda.koprocess

typealias ExitStatus = Int

fun execute(cmd: List<String>): ExitStatus =
    ProcessBuilder(cmd)
        .inheritIO()
        .start()
        .waitFor()
