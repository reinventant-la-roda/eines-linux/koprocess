plugins {
    kotlin("jvm") version "1.8.10" apply false
}

group = "com.reinventantlaroda.koprocess"
version = "0.0.0-SNAPSHOT"

subprojects {
    repositories {
        mavenCentral()
    }
}
