help:
	@echo "help	- show this help"
	@echo "dist	- generate a script for execute script"
	@echo "gradlew	- generate scripts"

dist:
	./gradlew installDist

gradlew: build.gradle.kts
	docker pull gradle:latest
	docker run \
		--rm \
		--user $(shell id -u):$(shell id -g) \
		--mount type=bind,source=${CURDIR},target=/home/gradle/bind \
		--workdir /home/gradle/bind \
		gradle:latest \
		gradle wrapper

build.gradle.kts:
	touch $@

.PHONY: \
	help \
	dist \
